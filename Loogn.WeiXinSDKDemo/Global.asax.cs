﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using Loogn.WeiXinSDK;

namespace Loogn.WeiXinSDKDemo
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            //设置全局配置
            WeiXin.ConfigGlobalCredential("appId", "appSecret");
            //设置AccessToken缓存存取方案
            Loogn.WeiXinSDK.WeiXin.ConfigAccessTokenCache((credential) =>
            {
                //这里大意了，的确需要使用绝对过期时间，感谢 小小草 http://git.oschina.net/liubiqu
                HttpRuntime.Cache.Add("WXAccessToken",
                    credential.access_token, null,
                    DateTime.Now.AddSeconds(credential.expires_in-30),
                    System.Web.Caching.Cache.NoSlidingExpiration,
                    System.Web.Caching.CacheItemPriority.Default, null);
            }, () =>
            {
                return HttpRuntime.Cache.Get("WXAccessToken") as string;
            });
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}